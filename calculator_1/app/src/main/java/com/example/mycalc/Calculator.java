package com.example.mycalc;

import java.io.IOException;

public class Calculator {
    public int first;
    public String operation;
    public int second;

    public Calculator(){
        first = 0;
        second = 0;
        operation = "";
    }

    public int calculate() throws IOException {
        switch (operation){
            case "*":
                return first * second;
            case "/":
                return first / second;
            case "+":
                return first + second;
            case "-":
                return first - second;
            case "=":
                return first;
            case "":
                return second;
            default:
                throw new IOException();
        }
    }
}
